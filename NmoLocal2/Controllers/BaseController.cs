﻿using System;
using System.Configuration;
using System.Web.Http;
using LenaSoft.Web.NmoLocal2.Filters;
using LenaSoft.Web.NmoLocal2.Models;
using LenaSoft.Web.NmoLocal2.Services;
using LenaSoft.Web.NmoLocal2.SharedLib;

namespace LenaSoft.Web.NmoLocal2.Controllers
{
    [RoutePrefix("api/base/1.0.0")]
    [NmoExceptionFilter]
    public class BaseController:ApiController
    {
        private readonly IBaseService _baseService;
        public BaseController()
        {
            var dbEngine = ConfigurationManager.AppSettings["DbEngine"];
            switch (dbEngine)
            {
                case "SqlServer":
                    _baseService = new BaseServiceSqlServer();
                    break;
                case "SqLite":
                    _baseService = new BaseServiceSqLite();
                    break;
                default:
                    throw new NmoException("","Неизвестный провайдер СУБД: "+dbEngine);
            }
        }

        [Route("get-conferences")]
        public ApiResult<ConferenceModel[]> GetConferenceLog()
        {
            var result = _baseService.GetConferences();
            return new ApiResult<ConferenceModel[]>(result);
        }

        [Route("get-clog-row")]
        public ApiResult<ConferenceLogModel> GetConferenceLogRow(int id)
        {
            var result = _baseService.GetConferenceLogRow(id);
            return new ApiResult<ConferenceLogModel>(result);
        }

        [Route("get-clog")]
        public ApiResult<ConferenceLogModel[]> GetConferenceLog(int id)
        {
            var result = _baseService.GetConferenceLog(id);
            return new ApiResult<ConferenceLogModel[]>(result);
        }
        [Route("search-clog")]
        public ApiResult<ConferenceLogModel[]> SearchConferenceLog(int id, string query)
        {
            var result = _baseService.Search(id, query);
            return new ApiResult<ConferenceLogModel[]>(result);
        }

        [HttpPost]
        [Route("set-clog-row")]
        public ApiResult SetConferenceLogRow([FromBody]ConferenceLogSetModel model)
        {
            /*if (!model.AgreePersonalDataPolicy)
            {
                return new ApiResult<ErrorData[]>(new[] { new ErrorData("", "Необходимо принять условия обработки и передачи персональных данных") { Subject = "AgreePersonalDataPolicy" } }) { Error= "Ошибки валидации",ErrorCode="0x015",Result=false };
            }*/
            model.AgreePersonalDataPolicy=true;
            if (model.SetRegistered.HasValue)
            {
                if (model.SetRegistered.Value)
                {
                    model.Registered = DateTime.Now;
                    model.Visited = DateTime.Now.AddMinutes(15);
                }
                else
                {
                    // model.Registered = null;
                    // model.Visited = null;
                }
                model.Visit = model.SetRegistered.Value;
            }
            _baseService.SetConferenceLog(model);
            var item = _baseService.GetConferenceLogRow(model.Id.Value);
            return new ApiResult<ConferenceLogModel>(item) {ItemId=model.Id.Value};
        }
        [HttpPost]
        [Route("table-insert")]
        public ApiResult<bool[]> SetConferenceLogRow([FromBody]ConferenceLogShortModel[] model,int id)
        {
            var result = _baseService.InsertConferenceLogRows(model,id);
            return new ApiResult<bool[]>(result);
        }
        

        [Route("get-nmo-codes")]
        public ApiResult<NmoCodeModel[]> GetNmoCodes()
        {
            var result = _baseService.GetNmoCodes();
            return new ApiResult<NmoCodeModel[]>(result);
        }
    }
}
