﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using LenaSoft.Web.NmoLocal2.SharedLib;

namespace LenaSoft.Web.NmoLocal2.Filters
{
    public class NmoExceptionFilter : ExceptionFilterAttribute
    {

        public NmoExceptionFilter()
        {
        }
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var result = new ApiResult<ErrorData[]>() { Result = false };
            var nmo = actionExecutedContext.Exception as NmoException;
            if (nmo != null)
            {
                result.Error = nmo.Message;
                result.ErrorCode = nmo.Code;
                result.Data = nmo.ErrorData;
                Console.WriteLine("NmoExceptionFilter: #{0}:{1}", result.ErrorCode, result.Error);
            }
            else
            {
                result.Error = actionExecutedContext.Exception.Message;
                result.ErrorCode = "";
                Console.WriteLine("NmoExceptionFilter: {0}", result.Error);
            }

            actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.OK);
            actionExecutedContext.Response.Content = new ObjectContent<ApiResult<ErrorData[]>>(result,new JsonMediaTypeFormatter());            
        }
    }
}
