﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using LenaSoft.Web.NmoLocal2.SharedLib;

namespace LenaSoft.Web.NmoLocal2.Filters
{
    public class NmoValidationFilter : Attribute, IActionFilter
    {
        public bool AllowMultiple { get; }

        public Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext actionContext,
            CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            if (!actionContext.ModelState.IsValid)
            {
                var errors = (
                    from error in actionContext.ModelState
                    select new ErrorData("", error.Value.Errors[0].ErrorMessage) {Subject = error.Key}).ToArray();
                throw new NmoException("0x015", "Ошибка валидации") {ErrorData = errors};
            }
            return null;
        }
    }
}
