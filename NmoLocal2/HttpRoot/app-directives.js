﻿(function () {
    var appDirectives;
    /*
     * Этот модуль регистрирует директивы
     */
    appDirectives = angular.module('appDirectives', []);

    // директива semantic-ui
    appDirectives.directive('appDropdown', function () {
        return {
            restrict: 'A',
            link: function (scope, elem) {
                return $(elem).dropdown();
            }
        };
    });
    // директива для выделения текста при клике
    appDirectives.directive('appSelectOnClick', ['$window', function ($window) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('click', function () {
                    if (!$window.getSelection().toString()) {
                        // Required for mobile Safari
                        this.setSelectionRange(0, this.value.length)
                    }
                });
            }
        };
    }]);

    // директива semantic-ui
    appDirectives.directive('appCheckbox', function () {
        return {
            restrict: 'A',
            link: function (scope, elem) {
                return $(elem).checkbox();
            }
        };
    });

    // директива semantic-ui
    appDirectives.directive('appFullModalShow', function () {
        return {
            restrict: 'A',
            link: function (scope, elem) {
                return $(elem).modal('setting', {
                    onShow: function () {
                        $(this).css({
                            'margin': '0',
                            'position': 'fixed',
                            'top': '0',
                            'bottom': '0',
                            'left': '0',
                            'right': '0',
                            'width': '100%'
                        });
                    },
                    closable: false
                }).modal('show');
            }
        };
    });
    appDirectives.directive('appBottomSidebarShow', function () {
        return {
            restrict: 'A',
            link: function (scope, elem) {
                return $(elem).sidebar('setting', { dimPage: false, closable:false}).sidebar('show');
            }
        };
    });
    // директива semantic-ui
    appDirectives.directive('appMenuTab', function () {
        return {
            restrict: 'A',
            link: function (scope, elem) {
                return $(elem).tab();
            }
        };
    });
    // директива semantic-ui
    appDirectives.directive('appPopup', function () {
        return {
            restrict: 'A',
            link: function (scope, elem) {
                return $(elem).popup();
            }
        };
    });
    appDirectives.directive('appPopupOn', function () {
        return function (scope, elem, attr) {
            scope.$on('appPopupOnShow', function (e, name) {
                if (name === attr.appPopupOn) {
                    $(elem[0]).popup({ on: 'hover', closable: true }).popup('toggle');
                }
            });
            scope.$on('appPopupOnHide', function (e, name) {
                if (name === attr.appPopupOn) {
                    $(elem[0]).popup({ on: 'manual' }).popup('hide');
                }
            });
        };
    });

    appDirectives.factory('popup', function ($rootScope, $timeout) {
        return {
            show: function (name) {
                $timeout(function () {
                    $rootScope.$broadcast('appPopupOnShow', name);
                });
            },
            hide: function (name) {
                $timeout(function () {
                    $rootScope.$broadcast('appPopupOnHide', name);
                });
            }
        }
    });
    // директива карусели
    appDirectives.directive('appCarousel', function ($interval) {
        return {
            restrict: 'A',
            link: function (scope, elem) {
                var carouselInit, carouselRollToPosition;
                carouselInit = function (carousel) {
                    var i, item, items, j, jcarousel, jcarouselItem, len, margin, width;
                    jcarousel = $(carousel);
                    jcarousel.data({
                        mouseOnIt: false
                    });
                    jcarousel.mouseover(function () {
                        return jcarousel.data({
                            mouseOnIt: true
                        });
                    });
                    jcarousel.mouseout(function () {
                        return jcarousel.data({
                            mouseOnIt: false
                        });
                    });
                    jcarouselItem = jcarousel.children().first();
                    items = jcarouselItem.children();
                    width = jcarousel.width();
                    margin = jcarouselItem.offset().left;
                    for (i = j = 0, len = items.length; j < len; i = ++j) {
                        item = items[i];
                        $(item).css({
                            position: 'absolute',
                            left: i * width + 'px',
                            width: width + 'px'
                        });
                    }
                    jcarousel.data({
                        position: 0
                    });
                    return $(carousel).each(function (i, elem) {
                        var nextBtn, prevBtn;
                        nextBtn = $('#' + elem.id + "NextBtn");
                        if (nextBtn) {
                            nextBtn.children().first().click(function () {
                                var position;
                                position = $(elem).data().position;
                                if (position >= $(elem).children().first().children().length - 1) {
                                    position = 0;
                                } else {
                                    position++;
                                }
                                carouselRollToPosition(elem, position);
                                return $(elem).data({
                                    skipNext: true
                                });
                            });
                        }
                        prevBtn = $('#' + elem.id + "PrevBtn");
                        if (prevBtn) {
                            return prevBtn.children().first().click(function () {
                                var position;
                                position = $(elem).data().position;
                                if (position > 0) {
                                    position--;
                                } else {
                                    position = $(elem).children().first().children().length - 1;
                                }
                                carouselRollToPosition(elem, position);
                                return $(elem).data({
                                    skipNext: true
                                });
                            });
                        }
                    });
                };
                carouselRollToPosition = function (carousel, position) {
                    var item, jcarousel, width;
                    jcarousel = $(carousel);
                    item = jcarousel.children().first();
                    width = jcarousel.width();
                    if (position === void 0) {
                        position = jcarousel.data().position;
                        item.animate({
                            left: -position * width
                        }, 300);
                    } else {
                        item.animate({
                            left: -position * width
                        }, 300);
                    }
                    return jcarousel.data({
                        position: position,
                        skipNext: false
                    });
                };
                carouselInit(elem);
                carouselRollToPosition(elem);
                $interval(function () {
                    var position;
                    if ($(elem).data().skipNext) {
                        $(elem).data({
                            skipNext: false
                        });
                        return;
                    } else {
                        if ($(elem).data().mouseOnIt) {
                            return;
                        }
                    }
                    position = $(elem).data().position;
                    if (position >= $(elem).children().first().children().length - 1) {
                        position = 0;
                    } else {
                        position++;
                    }
                    return carouselRollToPosition(elem, position);
                }, 5000);
                return $(window).resize(function () {
                    var i, item, items, j, len, position, width;
                    position = $(elem).data().position;
                    items = $(elem).children().first().children();
                    width = $(elem).width();
                    for (i = j = 0, len = items.length; j < len; i = ++j) {
                        item = items[i];
                        $(item).css({
                            position: 'absolute',
                            left: i * width + 'px',
                            width: width + 'px'
                        });
                    }
                    return carouselRollToPosition(elem, position);
                });
            }
        };
    });
    appDirectives.directive('focusOn', function () {
        return function (scope, elem, attr) {
            scope.$on('focusOn', function (e, name) {
                if (name === attr.focusOn) {
                    elem[0].focus();
                }
            });
        };
    });

    appDirectives.factory('focus', function ($rootScope, $timeout) {
        return function (name) {
            $timeout(function () {
                $rootScope.$broadcast('focusOn', name);
            });
        }
    });
    // директива пейджинга
    appDirectives.directive('appPaginator', function ($parse) {
        return {
            restrict: 'EA',
            replace: true,
            template: "<div class=\"ui small fluid pagination menu\"> <a ng-if=\"pIndex>1\" href=\"{{url+'?page='+(pIndex-1)}}\" class=\"item\"><i class=\"icon left arrow\"></i></a> <a href=\"{{url+'?page='+page}}\" class=\"item\" ng-repeat=\"page in list\" ng-bind=\"page\" ng-class=\"{'disabled':page==='...','active':page==index}\"></a> <a ng-if=\"pIndex<pPages\" href=\"{{url+'?page='+(pIndex+1)}}\" class=\"item\"><i class=\"icon right arrow\"></i></a> </div>",
            scope: {
                index: '@',
                pages: '@',
                pagesMax: '@',
                url: '@'
            },
            link: function (scope, elem, attrs) {
                var index, j, list, num1, num2, page, pages, pagesMax, ref, ref1;
                index = parseInt(scope.index);
                pages = parseInt(scope.pages);
                pagesMax = parseInt(scope.pagesMax);
                scope.pIndex = index;
                scope.pPages = pages;
                scope.pPagesMax = pagesMax;
                list = [];
                if (index <= pages && pages <= pagesMax) {
                    list = (function () {
                        var j, ref, results;
                        results = [];
                        for (page = j = 1, ref = pages; 1 <= ref ? j <= ref : j >= ref; page = 1 <= ref ? ++j : --j) {
                            results.push(page);
                        }
                        return results;
                    })();
                } else {
                    list.push(1);
                    num1 = index - pagesMax / 2 <= 2 ? 2 : index - pagesMax / 2;
                    num2 = index + pagesMax / 2 >= pages ? pages - 1 : index + pagesMax / 2;
                    if (index + 5 >= pages && num1 - pagesMax / 2 > 2) {
                        num1 = num2 - pagesMax;
                    }
                    if (num1 === 2 && num2 < pagesMax && pages >= pages) {
                        num2 = pagesMax;
                    }
                    if (num1 > 2) {
                        list.push('...');
                    }
                    for (page = j = ref = num1, ref1 = num2; ref <= ref1 ? j <= ref1 : j >= ref1; page = ref <= ref1 ? ++j : --j) {
                        list.push(page);
                    }
                    if (index + pagesMax / 2 < pages - 1) {
                        list.push('...');
                    }
                    list.push(pages);
                }
                return scope.list = list;
            }
        };
    });

}).call(this);
