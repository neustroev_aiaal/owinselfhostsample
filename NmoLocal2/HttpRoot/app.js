﻿var nmoLocal = angular.module('nmoLocal', ['ui.router', 'appDirectives']);


function apiService($http) {
    var urlBase = '/api/base/1.0.0/';

    function requestItem(url, method, callback, sendData) {
        this.url = url;
        this.method = method;
        this.sendData = sendData;
        this.callback = callback;

        this.request = function () {
            var urlParam, i, value;
            var req = { method: this.method, url: this.url };
            if (this.sendData) req.data = this.sendData;
            return req;
        }

        this.invoke = function () {
            $http(this.request()).then(this.callback);
        }
    }


    function getNmoCodes(callback, params, sendData) {
        return new requestItem(urlBase + 'get-nmo-codes', 'GET', callback, sendData);
    }
    function getConferences(callback, params, sendData) {
        return new requestItem(urlBase + 'get-conferences', 'GET', callback, sendData);
    }
    function getConferenceLog(callback, params, sendData) {
        return new requestItem(urlBase + 'get-clog?id=' + params.id, 'GET', callback, sendData);
    }
    function searchConferenceLog(callback, params, sendData) {
        return new requestItem(urlBase + 'search-clog?id=' + params.id + '&query=' + encodeURIComponent(params.query), 'GET', callback, sendData);
    }
    function getConferenceLogRow(callback, params, sendData) {
        return new requestItem(urlBase + 'get-clog-row?id=' + params.id, 'GET', callback, sendData);
    }
    function setConferenceLogRow(callback, params, sendData) {
        return new requestItem(urlBase + 'set-clog-row', 'POST', callback, sendData);
    }
    function tableInsert(callback, params, sendData) {
        return new requestItem(urlBase + 'table-insert?id=' + params.id, 'POST', callback, sendData);
    }
    return {
        base: {
            getNmoCodes: getNmoCodes,
            getConferences: getConferences,
            getConferenceLog: getConferenceLog,
            searchConferenceLog: searchConferenceLog,
            getConferenceLogRow: getConferenceLogRow,
            setConferenceLogRow: setConferenceLogRow,
            tableInsert: tableInsert
        }
    };
}

function apiMediumService() {
    return {
        make: function (action, cb, params, sendData) {
            function callback(response) {
                var data, errorCode, errorMessage, info, responseData, responseResult, result;
                responseResult = response.status >= 200 && response.status < 400;
                responseData = response.data;
                result = responseResult && typeof responseData === 'object' && responseData.result === true;
                if (responseData) {
                    data = responseData.data;
                    info = {
                        obsolete: responseData.obsolete,
                        itemId: responseData.itemId,
                        type: responseData.type
                    };
                } else {
                    info = {};
                }
                if (responseData.result === false) {
                    errorCode = responseData.errorCode;
                    errorMessage = responseData.error;
                }
                if (!responseResult) {
                    switch (response.status) {
                        case null:
                            errorCode = 'Ax000';
                            errorMessage = 'Не удалось получить запрошенные данные, повторите попытку через некоторое время...';
                            break;
                        case 400:
                            errorCode = 'Ax400';
                            errorMessage = 'Возникли ошибки при обработке запроса.';
                            break;
                        case 401:
                            errorCode = 'Ax401';
                            errorMessage = 'Необходимо войти в систему для продолжения.';
                            break;
                        case 403:
                            errorCode = 'Ax403';
                            errorMessage = 'Вам ограничен доступ к запрошенной информации';
                            break;
                        case 404:
                            errorCode = 'Ax404';
                            errorMessage = 'Информация не найдена.';
                            break;
                        case 500:
                        case 501:
                        case 502:
                        case 503:
                        case 505:
                            errorCode = 'Ax500';
                            errorMessage = 'Сервер не смог обработать запрос, повторите попытку через некоторое время...';
                    }
                }
                if (typeof (cb) === "function")
                    cb(result, data, errorCode, errorMessage, info);
            };

            if (typeof (action) === "function")
                return action(callback, params, sendData);
        }
    };
};
function structService() {
    function spTable(items, pos, openCb, deleteCb, createCb) {
        this.items = items;
        this.pos = pos;
        this.openCb = openCb;
        this.deleteCb = deleteCb;
        this.createCb = createCb;
        this.delete = function () {
            if (typeof this.deleteCb === "function")
                this.deleteCb()(this.pos >= 0);
        }
        this.create = function () {
            if (typeof this.createCb === "function")
                this.createCb();
        }
        this.open = function () {
            if (typeof this.openCb === "function")
                this.openCb();
        }
    }
    return {
        spTable: function (items, pos, openCb, deleteCb, createCb) { return new spTable(items, pos, openCb, deleteCb, createCb) }
    }
}
function modalService($templateRequest, $compile) {

    var data = {
        filesManager: {
            data: {
                path: '',
                selected: ''
            },
            onUploaded: function () {

            }
        },
        customModal: {}
    }
    function errorModal($scope, caption, message, onok, onretry) {
        $templateRequest('/partials/error-modal.html').then(function (template) {
            $compile($("#modal-container").html(template).contents())($scope);
            $('#errorModalOkBtn').on('click', onok);
            $('#errorModalRetryBtn').on('click', onretry);
            $('#errorModal').find('.header').text(caption);
            $('#errorModal').find('.content').text(message);
            $('#errorModal').modal('show');
        });
    }

    function customModal($scope, url, id, onok, oncansel) {
        $templateRequest(url).then(function (template) {
            $compile($("#modal-container").html(template).contents())($scope);
            $('#' + id + 'CanselBtn').on('click', oncansel);
            $('#' + id + 'OkBtn').on('click', function (e) {
                if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
                    $scope.$apply(function () { onok(data.customModal, e); });
                } else {
                    onok(data.customModal, e);
                }
            })
            $('#' + id).modal('show');
            //$(id).modal({
            //    onHidden: function () {
            //        $('body .modals').remove();
            //    }
            //});
        });
    }

    return {
        errorModal: errorModal,
        customModal: customModal,
        data: data
    }
}
function nmoCodes(apiService, apiMediumService) {
    var codes = { codes: [] }
    function reload() {
        function onload(result, data) {
            if (result) {
                codes.codes = data;
            }
        }
        apiMediumService.make(apiService.base.getNmoCodes, onload, null, null).invoke();
    }

    reload();
    codes.reload = reload;
    return codes;
}
function menu($window, $state) {

    this.back=function() {
        $window.history.back();
    }
}
function index(apiService, apiMediumService, modalService) {
    this.error = false;
    this.errorMessage = '';
    this.loading = true;

    this.apiService = apiService;
    this.apiMediumService = apiMediumService;
    this.modalService = modalService;
    var that = this;

    this.conferences = [];
    function onDataLoad(result, data, errorCode, errorMessage) {
        that.loading = false;
        that.error = !result;
        if (result) {
            that.conferences = data;
        } else {
            return that.errorMessage = errorMessage;
        }
    }


    this.load = function () {
        this.loading = true;
        var act = this.apiMediumService.make(this.apiService.base.getConferences, onDataLoad, null, null);
        act.invoke();

    }
    this.load();
}
function conferenceLog($location, $stateParams, apiService, apiMediumService, modalService, $state, structService, $scope,popup) {
    this.error = false;
    this.errorMessage = '';
    this.loading = true;
    this.search = {};
    this.$location = $location;
    this.$stateParams = $stateParams;
    this.apiService = apiService;
    this.apiMediumService = apiMediumService;
    this.$state = $state;
    this.structService = structService;
    this.$scope = $scope;
    this.id = $stateParams.id;
    this.ticketCode = '';
    this.modalService = modalService;
    var that = this;
    this.partTable = structService.spTable(null, -1,
        function () { $state.go('app.clog-row', { id: that.partTable.pos }) },
        function () { $state.go('app.clog-row', { id: that.partTable.pos, action: 'delete' }) },
        function () { $state.go('app.clog-row', { id: null, action: 'create' }) });
    function onDataLoad(result, data, errorCode, errorMessage) {
        that.loading = false;
        that.error = !result;
        if (result) {
            that.partTable.items = data;
        } else {
            return that.errorMessage = errorMessage;
        }
    }
    this.resetFilters=function() {
        this.search = {}
    }
    this.key = function (e) {
        if (e.keyCode === 27) {
            this.resetFilters();
        }
    }
    this.load = function () {
        this.loading = true;
        this.apiMediumService.make(this.apiService.base.getConferenceLog, onDataLoad, { id: this.id }, null).invoke();
    }
    this.load();

    this.searchTicket = function () {
        this.loading = true;
        function onFound(result, data, errorCode, errorMessage) {
            that.loading = false;
            if (result) {
                if (data.length === 1) {
                    $state.go('app.clog-row', { id: data[0].id });
                } else if (data.length > 1) {
                    that.modalService.errorModal(that.$scope, 'Обнаружено несколько таких кодов', errorMessage, function () { }, function () { that.edit() });
                } else if (data.length === 0) {
                    that.modalService.errorModal(that.$scope, 'Такой код не зарегистрирован', 'Попробуйте просканировать еще раз, или выполните поиск по ФИО.', function () { }, function () { that.edit() });
                }
            } else {
                that.modalService.errorModal(that.$scope, 'Что-то пошло не так.', errorMessage, function () { }, function () { that.edit() });
            }
        }
        this.apiMediumService.make(this.apiService.base.searchConferenceLog, onFound, { id: this.id, query: this.ticketCode }, null).invoke();
    }
    this.help = function () {
        popup.show('help');
    }
    this.helpClose = function () {
        popup.hide('help');
    }
    this.checkTickedCode=function(e) {
        if (e.keyCode === 8 || e.keyCode === 46) {
            this.ticketCode = '';
            e.preventDefault();
        }
    }
}

function conferenceLogRow($location, $stateParams, apiService, apiMediumService, modalService, $state, structService, $scope, $window, nmoCodes, focus) {
    this.error = false;
    this.errorMessage = '';
    this.loading = false;

    this.errors = {};
    this.errorMessages = {};
    this.editData = {};
    this.editLoading = false;
    this.editSuccess = $stateParams.action === 'editSuccess';
    this.$location = $location;
    this.$stateParams = $stateParams;
    this.id = $stateParams.id;
    this.apiService = apiService;
    this.apiMediumService = apiMediumService;
    this.$state = $state;
    this.structService = structService;
    this.$scope = $scope;
    this.$window = $window;
    this.action = $stateParams.action;
    this.modalService = modalService;
    this.nmoCodes = nmoCodes;
    this.showNmoCodes = false;
    var that = this;



    function onDataLoad(result, data, errorCode, errorMessage) {
        that.loading = false;
        that.error = !result;
        if (result) {
            that.editData = data;
            $state.current.data.pageTitle = 'Участник ' + data.fio + ' | НМО регистратура ';
            if (data.ticketCode && (data.nmoCode == undefined || data.nmoCode == '')) {
                focus('nmoCode');
            } else if (data.nmoCode) {
                focus('fio')
            }
        } else {
            return that.errorMessage = errorMessage;
        }
    }

    function editOnload(result, data, errorCode, errorMessage, info) {
        that.editLoading = false;
        that.editError = !result;
        that.editSuccess = result;
        if (result) {
            //that.$state.go('app.clog', { id: that.editData.conferenceId });
            if (that.action == 'create') {
                that.$state.go('app.clog-row', { id: info.itemId, action: null });
            } else {
                onDataLoad(result, data, errorCode, errorMessage);
            }
        } else {
            if (errorCode == "0x015") {
                for (var i = 0; i < data.length; i++) {
                    that.errors[data[i].subject] = true;
                    that.errorMessages[data[i].subject] = data[i].error;
                }
            } else {
                that.editErrorMessage = errorMessage;
                that.modalService.errorModal(that.$scope, 'Ошибка при сохранении записи', errorMessage, function () { }, function () { that.edit() });
            }
        }
    }
    this.load = function () {
        this.loading = true;
        this.apiMediumService.make(this.apiService.base.getConferenceLogRow, onDataLoad, { id: this.id }, null).invoke();
    }
    if (this.action !== 'create') {
        this.load();
        focus('ticketCode');
    } else {
        focus('fio');
        this.editData = {
            conferenceId: this.$stateParams.intoId,
            agreePersonalDataPolicy: false,
            visit: false,
            points: 0,
            lector: false,
            emailUnsubscribe: false,
            setRegistered: false,
            setVisited: false,
            type: 0
        }
        $state.current.data.pageTitle = 'Регистрация нового участника | НМО регистратура ';
    }
    this.types = [{ code: 0, text: 'Слушатель' }, { code: 1, text: 'Слушатель пленарного заседания' }, { code: 2, text: 'Лектор' }]
    this.edit = function () {
        this.editLoading = true;
        this.errors = {}
        this.errorMessages = {}
        this.apiMediumService.make(this.apiService.base.setConferenceLogRow, editOnload, null, this.editData).invoke();
    }
    this.editCansel = function () {
        this.$state.go('app.clog', { id: this.editData.conferenceId });
    }
    this.nmoCodeSearch = { code: '' };
    this.showNmoCodeBrowser = function () {
        this.showNmoCodes = true;
        focus("nmoCodeSearch");
    }
    this.nmoCodeSelect = function (nmoCode) {
        this.editData.nmoCode = nmoCode;
        this.showNmoCodes = false;
        this.edit();
    }
    this.checkTickedCode = function (e) {
        if (e.keyCode === 8 || e.keyCode === 46) {
            this.editData.ticketCode = '';
            e.preventDefault();
        }
    }
    this.checkStrict = function () {
        this.nmoCodeSearchStrict = this.nmoCodeSearch.code.length > 0;
    }
    this.setRegistered = function (val) {
        this.editData.setRegistered = val == true
        this.edit()
    }
    this.setVisited = function (val) {
        this.editData.setVisited = val == true
        this.edit()
    }
    this.printer = function (val) {
       
            var fio = this.editData.fio;
            var specialization = this.editData.speciality;
            var organisation = this.editData.organization;
            var code = this.editData.ticketCode;

            var html = "<div> <center> <h4>" + "ФИО " + fio + "</h4> <h6>" + "Специальность " + specialization + "</h6> <h6>" + 'Организация ' + organisation + "</h6> <h6>" + code + "</h6> </center> </div>"

            var printContents = html;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
            window.history.go(-1);
            window.location.reload();
        }
    }

    function tableInserter(apiService, apiMediumService, modalService, $state, $stateParams, $scope) {
        this.data = [];
        this.dataString = '';
        this.error = false;
        this.parseError = false;
        this.loading = false;
        this.apiService = apiService;
        this.apiMediumService = apiMediumService;
        this.modalService = modalService;
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.$scope = $scope;
        this.id = $stateParams.id;
        this.change = function () {
            var s, part, items, error;
            items = this.dataString.split(/\r?\n/);
            this.data = [];
            this.parseError = false;
            var code, nmoCode, ticketCode, fio, organization, speciality, phone, email;
            for (var i = 0; i < items.length; i++) {
                s = items[i].split("\t");
                //error = items[i] === '' || s[0] === '' || s[1] === '' || s[2] === '';
                code = s[0]
                nmoCode = s[1];
                ticketCode = s[2];
                fio = s[3];
                organization = s[4];
                speciality = s[5];
                phone = s[6];
                email = s[7];
                this.data.push({ code: code, nmoCode: nmoCode, ticketCode: ticketCode, fio: fio, organization: organization, speciality: speciality, phone: phone, email: email });
            }
        }
        var that = this;
        function onDone(result, data, errorCode, errorMessage) {
            that.error = !result;
            if (result) {
                var error;
                for (var i = 0; i < data.length; i++) {
                    error = data[i] === true || error;
                    that.data[i].error = data[i] === true;
                }
                if (error) {
                    that.modalService.errorModal(that.$scope, 'Данные частично сохранены', 'Данные успешно сохранены за исключением нескольких помеченных записей, см. таблицу для подробностей', function () { }, function () { });
                } else {
                    that.$state.go('app.clog', { id: that.id });
                }
            } else {
                that.editErrorMessage = errorMessage;
                that.modalService.errorModal(that.$scope, 'Ошибка при сохранении записи', errorMessage, function () { }, function () { that.next() });
            }
        }
        this.next = function () {
            if (this.data.length > 0) {
                this.loading = false;
                this.error = false;
                this.apiMediumService.make(this.apiService.base.tableInsert, onDone, { id: this.id }, this.data).invoke();
            }
        }

    }

    nmoLocal.factory('apiService', ['$http', apiService]);
    nmoLocal.factory('apiMediumService', [apiMediumService]);
    nmoLocal.factory('structService', [structService]);
    nmoLocal.factory('nmoCodes', ['apiService', 'apiMediumService', nmoCodes]);

    nmoLocal.factory('modalService', ['$templateRequest', '$compile', modalService]);
    nmoLocal.controller('menu', ['$window', '$state', menu]);
    nmoLocal.controller('index', ['apiService', 'apiMediumService', 'modalService', index]);
    nmoLocal.controller('conferenceLog', ['$location', '$stateParams', 'apiService', 'apiMediumService', 'modalService', '$state', 'structService', '$scope', 'popup', conferenceLog]);
    nmoLocal.controller('conferenceLogRow', ['$location', '$stateParams', 'apiService', 'apiMediumService', 'modalService', '$state', 'structService', '$scope', '$window', 'nmoCodes', 'focus', conferenceLogRow]);
    nmoLocal.controller('tableInserter', ['apiService', 'apiMediumService', 'modalService', '$state', '$stateParams', '$scope', tableInserter]);
    nmoLocal.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(false);
        $stateProvider.state('app', {
            "abstract": true,
            views: {
                "wrapper": {
                    templateUrl: "/Partials/wrapper.html"
                }
            }
        });
        $stateProvider.state('login', {
            url: "/login",
            views: {
                "wrapper": {
                    templateUrl: "/Partials/login.html",
                    controller: 'loginController as lc'
                }
            },
            data: { pageTitle: 'Вход в систему | НМО регистратура' }
        });
        $stateProvider.state('app.index', {
            url: "/",
            views: {
                page: {
                    templateUrl: "/Pages/index.html",
                    controller: "index",
                    controllerAs: "ic"
                }
            },
            data: { pageTitle: 'Главная | НМО регистратура' }
        });
        $stateProvider.state('app.clog', {
            url: "/clog?id",
            views: {
                page: {
                    templateUrl: "/Pages/clog.html",
                    controller: "conferenceLog",
                    controllerAs: 'cc'
                }
            },
            data: { pageTitle: 'Список участников | НМО регистратура' }
        });
        $stateProvider.state('app.clog-insert', {
            url: "/clog-insert?id",
            views: {
                page: {
                    templateUrl: "/Pages/clog-insert.html",
                    controller: "tableInserter",
                    controllerAs: 'ti'
                }
            },
            data: { pageTitle: 'Список участников | НМО регистратура' }
        });
        $stateProvider.state('app.clog-row', {
            url: "/clog-row?id&action&intoId",
            views: {
                page: {
                    templateUrl: "/Pages/clog-row.html",
                    controller: "conferenceLogRow",
                    controllerAs: 'cc'
                }
            },
            data: { pageTitle: 'Участник | НМО регистратура' }
        });
        $stateProvider.state('error', {
            url: "/error",
            "abstract": true,
            views: {
                "wrapper": {
                    templateUrl: "/Partials/error.html"
                }
            }
        });
        $stateProvider.state('error.unauthorized', {
            url: "/401",
            views: {
                page: {
                    templateUrl: "/ErrorPages/unauthorized.html"
                }
            },
            data: { pageTitle: 'В доступе отказано | НМО регистратура' }
        });
        $stateProvider.state('error.denied', {
            url: "/403",
            views: {
                page: {
                    templateUrl: "/ErrorPages/denied.html"
                }
            },
            data: { pageTitle: 'В доступе отказано | НМО регистратура' }
        });
        $stateProvider.state('error.notfound', {
            url: "/404",
            views: {
                page: {
                    templateUrl: "/ErrorPages/notfound.html"
                }
            },
            data: { pageTitle: 'Объект не обнаружен | НМО регистратура' }
        });
        $urlRouterProvider.otherwise("/");
    });


    function appRun($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.$on('$stateNotFount', function (e, unfoundState, fromState, fromParams) {
            $state.go('error.notfound');
        });
        $rootScope.$on('$stateChangeSuccess', function (e, toState, toParams, fromState, fromParams, error) {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }
    nmoLocal.run(['$rootScope', '$state', '$stateParams', appRun]);



    $(document).on("keydown",
        function (e) {
            if (e.keyCode === 70 && e.ctrlKey && $('#clogSearchFio').length === 1) {
                $('#clogSearchFio').focus();
                e.preventDefault();
            }
            if (e.keyCode === 68 && e.ctrlKey && $('#clogScanCode').length === 1) {
                $('#clogScanCode').focus();
                e.preventDefault();
            }
            if (e.keyCode === 88 && e.ctrlKey && $('#clogNewPart').length === 1) {
                $('#clogNewPart').click();
                e.preventDefault();
            }
        }
    );