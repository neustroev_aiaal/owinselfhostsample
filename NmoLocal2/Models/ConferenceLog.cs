﻿using System;
using System.Runtime.Serialization;
using System.Data.SQLite;

namespace LenaSoft.Web.NmoLocal2.Models
{
    [DataContract]
    public class ConferenceLogModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "conferenceId")]
        public int ConferenceId { get; set; }

        [DataMember(Name = "ticketCode")]
        public string TicketCode { get; set; }

        [DataMember(Name = "nmoCode")]
        public string NmoCode { get; set; }

        [DataMember(Name = "fio")]
        public string Fio { get; set; }

        [DataMember(Name = "organization")]
        public string Organization { get; set; }

        [DataMember(Name = "speciality")]
        public string Speciality { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "agreePersonalDataPolicy")]
        public bool AgreePersonalDataPolicy { get; set; }


        [DataMember(Name = "actorComment")]
        public string ActorComment { get; set; }

        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        [DataMember(Name = "visit")]
        public bool Visit { get; set; }

        [DataMember(Name = "points")]
        public int Points { get; set; }

        [DataMember(Name = "lector")]
        public bool Lector { get; set; }

        [DataMember(Name = "type")]
        public int Type { get; set; }
        private string _registered;

        [IgnoreDataMember]
        public DateTime? Registered { get; set; }

        [DataMember(Name = "registeredString")]
        public string RegisteredString
        {
            get { return _registered ?? (_registered = Registered?.ToString("dd.MM.yyyy HH:mm")); }
            set { _registered = value; }
        }


        private string _visited;

        [IgnoreDataMember]
        public DateTime? Visited { get; set; }

        [DataMember(Name = "visitedString")]
        public string VisitedString
        {
            get { return _visited ?? (_visited = Visited?.ToString("dd.MM.yyyy HH:mm")); }
            set { _visited = value; }
        }

        [DataMember(Name = "emailUnsubscribe")]
        public bool EmailUnsubscribe { get; set; }

        [DataMember(Name = "unsubscribeLink")]
        public string UnsubscribeLink { get; set; }
    }
}
