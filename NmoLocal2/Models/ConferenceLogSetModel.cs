﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace LenaSoft.Web.NmoLocal2.Models
{
    public class ConferenceLogShortModel
    {
        [DataMember(Name = "code")]
        public string Code { get; set; }
        [DataMember(Name = "nmoCode")]
        public string NmoCode { get; set; }
        [DataMember(Name = "ticketCode")]
        public string TicketCode { get; set; }
        //[Required(ErrorMessage = "Обязательное поле")]
        [DataMember(Name = "fio")]
        public string Fio { get; set; }
        [DataMember(Name = "organization")]
        //[Required(ErrorMessage = "Обязательное поле")]
        public string Organization { get; set; }
        [DataMember(Name = "speciality")]
        //[Required(ErrorMessage = "Обязательное поле")]
        public string Speciality { get; set; }
        [DataMember(Name = "phone")]
        public string Phone { get; set; }
        [DataMember(Name = "email")]
        public string Email { get; set; }
    }
    [DataContract]
    public class ConferenceLogSetModel
    {
        [DataMember(Name = "id")]
        public int? Id { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DataMember(Name = "conferenceId")]
        public int ConferenceId { get; set; }

        [DataMember(Name = "ticketCode")]
        public string TicketCode { get; set; }

        [DataMember(Name = "nmoCode")]
        public string NmoCode { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DataMember(Name = "fio")]
        public string Fio { get; set; }

        [DataMember(Name = "organization")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Organization { get; set; }

        [DataMember(Name = "speciality")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Speciality { get; set; }

        [DataMember(Name = "points")]
        public int Points { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "agreePersonalDataPolicy")]
        //[Required(ErrorMessage = "Обязательное поле")]
        public bool AgreePersonalDataPolicy { get; set; }
        
        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        [DataMember(Name = "visit")]
        [Required(ErrorMessage = "Обязательное поле")]
        public bool Visit { get; set; }

        //[DataMember(Name = "lector")]
        //[Required(ErrorMessage = "Обязательное поле")]
        //public bool Lector { get; set; }

        [DataMember(Name = "type")]
        [Required(ErrorMessage = "Обязательное поле")]
        public int Type{ get; set; }

        [IgnoreDataMember]
        public DateTime? Registered { get; set; }

        [IgnoreDataMember]
        public DateTime? Visited { get; set; }

        [DataMember(Name = "setRegistered")]
        public bool? SetRegistered { get; set; }
        [DataMember(Name = "setVisited")]
        public bool? SetVisited { get; set; }

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    if (!AgreePersonalDataPolicy)
        //        yield return new ValidationResult("Необходимо принять условия обработки и передачи персональных данных", new[] { "AgreePersonalDataPolicy" });
        //}
    }
}