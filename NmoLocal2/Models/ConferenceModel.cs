﻿using System.Runtime.Serialization;

namespace LenaSoft.Web.NmoLocal2.Models
{
    [DataContract]
    public class ConferenceModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "shortName")]
        public string ShortName { get; set; }

        [DataMember(Name = "dates")]
        public string Dates { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }

        [DataMember(Name = "messageTemplate")]
        public string MessageTemplate { get; set; }

        [DataMember(Name = "pagesAddress")]
        public string PageAddress { get; set; }

        [DataMember(Name = "signName")]
        public string SignName { get; set; }
    }
}