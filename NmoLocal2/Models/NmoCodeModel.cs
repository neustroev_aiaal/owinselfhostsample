﻿using System.Runtime.Serialization;

namespace LenaSoft.Web.NmoLocal2.Models
{
    [DataContract]
    public class NmoCodeModel
    {
        [DataMember(Name = "code")]
        public string Code { get; set; }
        [DataMember(Name = "nmoCode")]
        public string NmoCode { get; set; }
    }
}