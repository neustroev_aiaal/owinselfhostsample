﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using Dapper;
using Microsoft.Owin.Hosting;

namespace LenaSoft.Web.NmoLocal2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var baseAddress = ConfigurationManager.AppSettings["Http:Listen"];
            Console.WriteLine($"Запуск приложения по адресу {baseAddress}");
            WebApp.Start<Startup>(url: baseAddress);
            var work = true;
            while (work)
            {
                Console.Write(">");
                var command = Console.ReadLine();
                switch (command)
                {
                    case "help":
                        Console.WriteLine("Доступные команды:\r\n\tsqlite\tзапуск sqlite сскриптов.\r\n\thelp\tвывод этого экрана.");
                        break;
                    case "sqlite":
                        SqliteProxy.Query();
                        break;
                    case "exit":
                        work = false;
                        break;
                    default:
                        Console.WriteLine("Команда нераспознана. Введите help для помощи.");
                        break;
                }
            }
        }
    }

    public class SqliteProxy
    {
        public static SQLiteConnection Connection = new SQLiteConnection("db.sqlite");

        public static void Query()
        {
            if (Connection == null)
            {
                Console.WriteLine("SqLite незапущен.");
                return;
            }
            var start = true;
            while (true)
            {
                Console.Write("sqlite>");
                if (start)
                {
                    start = false;
                    const string tablesQuery = "SELECT name FROM sqlite_master WHERE type = \"table\"";
                    Console.WriteLine(tablesQuery);
                    _query(tablesQuery);
                    continue;
                }

                var query = Console.ReadLine();
                if (query == "\\q")
                    return;
                _query(query);
            }
        }

        private static void _query(string query)
        {
            IDataReader data2 = null;
            try
            {
                data2 = Connection.ExecuteReader(query);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            try
            {
                if (data2 == null)
                {
                    Console.WriteLine("Последовательность не содержит элементов.");
                    return;
                }
                //bool first = true;
                var count = 0;
                Console.WriteLine("".PadLeft(Console.WindowWidth, '_'));
                while (data2.Read())
                {
                    var line = Console.CursorTop;
                    var pos = 0;
                    var width = Console.WindowWidth / data2.FieldCount;
                    var values = new object[data2.FieldCount];
                    data2.GetValues(values);
                    foreach (var value in values)
                    {
                        Console.SetCursorPosition(pos, line);
                        if (value == null)
                            Console.Write("<null>");
                        else
                        {
                            var str = value.ToString();
                            if (str.Length > width - 2) str = str.Substring(0, width);
                            Console.Write("|" + str);
                        }
                        pos += width;
                    }
                    Console.WriteLine();
                    Console.WriteLine("".PadLeft(Console.WindowWidth, '_'));
                    count++;
                };
                Console.WriteLine($"Результат: {count} строк");
            }
            catch (Exception e)
            {
                Console.WriteLine("sqlite exception: " + e.Message);
            }
        }
    }    

}
