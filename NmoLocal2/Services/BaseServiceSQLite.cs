﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using Dapper;
using System.Globalization;
using LenaSoft.Web.NmoLocal2.Models;
using System.Collections.Generic;

namespace LenaSoft.Web.NmoLocal2.Services
{

    public class BaseServiceSqLite : IBaseService
    {

        public BaseServiceSqLite()
        {
            var file = ConfigurationManager.AppSettings["SqLite:File"];
            _connection = new SQLiteConnection("Data Source=" + file);
            SqliteProxy.Connection = _connection;
            _installDatabase();

        }

        private readonly SQLiteConnection _connection;

        private void _installDatabase()
        {
            _connection.Open();
            const string queryConference = @"
CREATE TABLE IF NOT EXISTS Conference(
	Id int NOT NULL,
	Name varchar(600) NOT NULL,
	ShortName varchar(300) NULL,
	Dates varchar(50) NULL,
	Address varchar(600) NULL,
	MessageTemplate varchar(250) NULL DEFAULT ('BasicNotification'),
	PageAddress varchar(600) NULL,
	SignName varchar(150) NULL
);";
            const string queryConferenceLog = @"
CREATE TABLE IF NOT EXISTS ConferenceLog(
	Id int PRIMARY KEY NOT NULL,
	ConferenceId int NOT NULL,
	TicketCode varchar(50) NULL,
	NmoCode varchar(50) NULL,
	Fio varchar(150) NOT NULL,
	Organization varchar(300) NOT NULL,
	Speciality varchar(100) NOT NULL,
	Phone varchar(75) NULL,
	Email varchar(300) NULL,
	AgreePersonalDataPolicy bit NOT NULL DEFAULT (1),
	ActorComment varchar(1024) NULL,
	Comment varchar(1024) NULL,
	Visit bit NOT NULL,
	Points int NULL,
	Lector bit NOT NULL DEFAULT (0),
	Registered datetime NULL DEFAULT (datetime('now','localtime')),
	Visited datetime NULL,
	EmailUnsubscribe bit NOT NULL DEFAULT (0),
	UnsubscribeLink varchar(1024) NULL,
	Type int NULL DEFAULT (0),
	RegisterType int NOT NULL DEFAULT (0)
);";
            const string queryNmoCodes = @"
CREATE TABLE IF NOT EXISTS NmoCodes(
	NmoCode varchar(50) NOT NULL,
	Code varchar(50) NOT NULL,
	TicketCode varchar(50) NULL
);";

            try
            {
                _connection.Execute(queryConference);
                _connection.Execute(queryConferenceLog);
                _connection.Execute(queryNmoCodes);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public ConferenceModel[] GetConferences()
        {
            const string query = @"SELECT [Id],[Name],[ShortName],[Dates],[Address],[MessageTemplate],[PageAddress],[SignName] FROM Conference";
            var result = _connection.Query<ConferenceModel>(query).ToArray();
            return result;
        }

        public ConferenceLogModel GetConferenceLogRow(int id)
        {
            const string query = @"SELECT Id, ConferenceId,TicketCode,NmoCode,Fio,Organization,Type,Points,Phone,Lector,Speciality,Email,AgreePersonalDataPolicy,strftime('%d-%m-%Y', Registered) as RegisteredString,strftime('%d-%m-%Y', Visited) as Visited FROM ConferenceLog WHERE Id = @Id";
            var result1 = _connection.Query<dynamic>(query, new { Id = id });
            var result = result1.Select(s => new ConferenceLogModel()
            {
                Id = s.Id,
                ConferenceId = s.ConferenceId,
                TicketCode = s.TicketCode,
                NmoCode = s.NmoCode,
                Fio = s.Fio,
                Organization = s.Organization,
                Speciality = s.Speciality,
                RegisteredString = s.RegisteredString,
                VisitedString = s.VisitedString,
                AgreePersonalDataPolicy = s.AgreePersonalDataPolicy,
                Email = s.Email,
                Lector = s.Lector,
                Type = s.Type,
                Phone = s.Phone,
                Points = s.Points
            }).FirstOrDefault();
            return result;
        }

        public ConferenceLogModel[] GetConferenceLog(int id)
        {
            const string query = @"SELECT Id, ConferenceId,TicketCode,NmoCode,Fio,Organization,Type,Points,Phone,Lector,Speciality,Email,AgreePersonalDataPolicy,strftime('%d-%m-%Y', Registered) as RegisteredString,strftime('%d-%m-%Y', Visited) as Visited FROM ConferenceLog WHERE ConferenceId = @Id";
            var result1 = _connection.Query<dynamic>(query, new {Id = id});
            var result = result1.Select(s => new ConferenceLogModel()
            {
                Id = s.Id,
                ConferenceId = s.ConferenceId,
                TicketCode = s.TicketCode,
                NmoCode = s.NmoCode,
                Fio = s.Fio,
                Organization = s.Organization,
                Speciality = s.Speciality,
                RegisteredString = s.RegisteredString,
                VisitedString = s.VisitedString,
                AgreePersonalDataPolicy = s.AgreePersonalDataPolicy,
                Email = s.Email,
                Lector = s.Lector,
                Type = s.Type,
                Phone = s.Phone,
                Points = s.Points
            }).ToArray();
            return result;
        }

        public void SetConferenceLog(ConferenceLogSetModel model)
        {
            if (model.Id.HasValue) //
            {
                //Id,ConferenceId,TicketCode,NmoCode,Fio,Organization,Speciality,Phone,Email,AgreePersonalDataPolicy,ActorComment,Comment,Visit,Points,Lector,Registered,Visited
                var query = @"UPDATE ConferenceLog 
SET
    ConferenceId = @ConferenceId,
    TicketCode = @TicketCode,
    NmoCode  = @NmoCode,
	Fio  = @Fio,
	Organization = @Organization,
	Speciality = @Speciality,
	Phone = @Phone ,
	Email = @Email,
	AgreePersonalDataPolicy = @AgreePersonalDataPolicy,
	Comment = @Comment,
	Visit = @Visit,
	Points = @Points,
	" + (model.SetRegistered.HasValue?"Registered=@Registered,Visited=@Visited,Visit=@Visit":"")
+@"
	Type = @Type
WHERE Id= @Id
";                
                _connection.Execute(query, new
                {
                    model.Id,
                    model.ConferenceId,
                    model.TicketCode,
                    model.NmoCode,
                    model.Fio,
                    model.Organization,
                    model.Speciality,
                    model.Points,
                    model.Phone,
                    model.Email,
                    model.AgreePersonalDataPolicy,
                    model.Comment,
                    model.Visit,
                    model.Type,
                    model.Registered,
                    model.Visited
                }

               );
            }
            else
            {
                var id = _connection.ExecuteScalar<int>("SELECT MAX(Id) FROM ConferenceLog");
                id++;
                const string query =
                @"INSERT INTO ConferenceLog(Id,ConferenceId,TicketCode,NmoCode,Fio,Organization,Speciality,Phone,Email,AgreePersonalDataPolicy,Comment,Visit,Points,Type,Registered,Visited,Lector)
	VALUES (@Id,@ConferenceId,@TicketCode,@NmoCode,@Fio,@Organization,@Speciality,@Phone,@Email,@AgreePersonalDataPolicy,@Comment,@Visit,@Points,@Type,@Registered,@Visited,@Lector)
	";
                _connection.Execute(query,
               new
               {
                   Id=id,
                   model.ConferenceId,
                   model.TicketCode,
                   model.NmoCode,
                   model.Fio,
                   model.Organization,
                   model.Speciality,
                   model.Points,
                   model.Phone,
                   model.Email,
                   model.AgreePersonalDataPolicy,
                   model.Comment,
                   model.Visit,
                   model.Type,
                   model.Visited,
                   model.Registered,
                   Lector = model.Type == 2
               }
               );
                model.Id = id;
            }
        }

        public ConferenceLogModel[] Search(int id, string query)
        {
           // const string sql = @"SELECT [Id],[ConferenceId],[TicketCode],[NmoCode],[Fio],[Organization],[Speciality],[Phone],[Email],[AgreePersonalDataPolicy],[ActorComment],[Comment],[Visit],[Points],[Lector],[Registered],[Visited],[EmailUnsubscribe],[UnsubscribeLink] FROM Conference 
           // WHERE Fio LIKE {0}";
            ConferenceLogModel[] result = null;
            //result = _connection.Query<ConferenceLogModel>(String.Format(sql,query));
            //var result = _connection.Query<ConferenceLogModel>(sql).ToArray();

            return result;
        }

        public NmoCodeModel[] GetNmoCodes()
        {
            const string query = @"SELECT NmoCode, Code FROM NmoCodes";
            var result = _connection.Query<NmoCodeModel>(query).ToArray();
            return result;
        }

        public bool[] InsertConferenceLogRows(ConferenceLogShortModel[] model, int id)
        {
            const string query = @"INSERT INTO ConferenceLog(ConferenceId, NmoCode,TicketCode,Fio,Organization,Speciality,Phone,Email) 
                                    VALUES (@Id,@Code,@NmoCode,@TicketCode,@Fio,@Organization,@Speciality,@Phone,@Email)";
            var errors = new bool[model.Length];
            for (int i = 0; i < model.Length; i++)
            {
                var item = model[i];
                try
                {
                    _connection.Execute(query,
                        new
                        {
                            Id = id,
                            Code = item.Code,
                            NmoCode = item.NmoCode,
                            TicketCode = item.TicketCode,
                            Fio = item.Fio,
                            Organization = item.Organization,
                            Speciality = item.Speciality,
                            Phone = item.Phone,
                            Email = item.Email
                        }
                        );
                }
                catch (Exception e)
                {
                    if (i == 1)
                        Console.WriteLine(e.Message);
                    errors[i] = true;
                }
            }
            return errors;
        }

    }

    //public class DateTimeNullable : SqlMapper.TypeHandler<DateTime?>
    //{
    //    public override void SetValue(IDbDataParameter parameter, DateTime? value)
    //    {
    //        parameter.
    //    }

    //    public override DateTime? Parse(object value)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}
