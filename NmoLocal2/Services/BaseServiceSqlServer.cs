﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

using LenaSoft.Web.NmoLocal2.Models;
using LenaSoft.Web.NmoLocal2.SharedLib;

namespace LenaSoft.Web.NmoLocal2.Services
{
    public class BaseServiceSqlServer : IBaseService
    {
        private readonly SqlConnection _connection;

        public BaseServiceSqlServer()
        {
            _connection = new SqlConnection(ConfigurationManager.AppSettings["SqlServer:connection-string"]);
        }

        public ConferenceModel[] GetConferences()
        {
            var result = _connection.Query<ConferenceModel>("[dbo].[sp_base_get_conferences]",  commandType: CommandType.StoredProcedure).ToArray();
            return result;
        }
        
        public ConferenceLogModel GetConferenceLogRow(int id)
        {
            var result = _connection.Query<ConferenceLogModel>("[dbo].[sp_base_get_clog_row]", new { Id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            return result;
        }

        public ConferenceLogModel[] GetConferenceLog(int id)
        {
            var result = _connection.Query<ConferenceLogModel>("[dbo].[sp_base_get_clog]", new { Id = id }, commandType: CommandType.StoredProcedure).ToArray();
            return result;
        }

        private string _getNmoByTicket(string ticket)
        {
            var result = _connection.ExecuteScalar<string>("[dbo].[sp_base_get_nmo_by_ticket]", new { Ticket = ticket }, commandType: CommandType.StoredProcedure);
            return result;
        }

        public void SetConferenceLog(ConferenceLogSetModel model)
        {
            ConferenceLogModel item;
            if (model.Id.HasValue) {
                item = this.GetConferenceLogRow(model.Id.Value);
                model.Registered = item.Registered;
                model.Visited = item.Visited;
                model.Visit = item.Visit;
            }
            if (!string.IsNullOrEmpty(model.TicketCode))
            {
                var collizions = _connection.Query<ConferenceLogModel>("[dbo].[sp_base_check_ticket]", new {Id=model.Id, ConferenceId=model.ConferenceId, Ticket = model.TicketCode }, commandType: CommandType.StoredProcedure).ToArray();
                if (collizions.Length > 0)
                {
                    var agr = collizions.Select(s=>s.Fio).Aggregate("Такой ШТРИХ-Код уже зарегистрирован у слушателей: ", (a, b) => a + b+", ",r=>r.Substring(0,r.Length-2)+".");
                    throw new NmoException("", agr);
                }
            }
            if (!string.IsNullOrEmpty(model.TicketCode) && string.IsNullOrEmpty(model.NmoCode))
            {
                var nmo = _getNmoByTicket(model.TicketCode);
                if (!string.IsNullOrEmpty(nmo))
                    model.NmoCode = nmo;
            }
            if (!string.IsNullOrEmpty(model.NmoCode))
            {
                var collizions = _connection.Query<ConferenceLogModel>("[dbo].[sp_base_check_nmo_code]", new { Id = model.Id, NmoCode =model.NmoCode }, commandType: CommandType.StoredProcedure).ToArray();
                if (collizions.Length > 0)
                {
                    var agr = collizions.Select(s => s.Fio).Aggregate("Такой код подтверждения уже привязан у слушателей: ", (a, b) => a + b + ", ", r => r.Substring(0, r.Length - 2) + ".");
                    throw new NmoException("", agr);
                }
            }
            var id = _connection.Query<int>("[dbo].[sp_base_set_clog_row]", new {
                Id = model.Id,
                ConferenceId = model.ConferenceId,
                TicketCode = model.TicketCode,
                NmoCode = model.NmoCode,
                Fio = model.Fio,
                Organization = model.Organization,
                Speciality = model.Speciality,
                Points = model.Points,
                Phone = model.Phone,
                Email = model.Email,
                AgreePersonalDataPolicy = model.AgreePersonalDataPolicy,
                Comment = model.Comment,
                Visit = model.Visit,
                Lector = model.Type==2,
                Type = model.Type,
                Registered = model.Registered,
                Visited = model.Visited,
            }, commandType: CommandType.StoredProcedure).Single();
            model.Id = id;
        }
        public ConferenceLogModel[] Search(int id,string query)
        {
            var result = _connection.Query<ConferenceLogModel>("[dbo].[sp_base_search_clog]", new {Id=id, Query = query }, commandType: CommandType.StoredProcedure).ToArray();
            return result;
        }
        
        public NmoCodeModel[] GetNmoCodes()
        {
            var result = _connection.Query<NmoCodeModel>("[dbo].[sp_base_get_nmo_codes]", commandType: CommandType.StoredProcedure).ToArray();
            return result;
        }

        public bool[] InsertConferenceLogRows(ConferenceLogShortModel[] model, int id)
        {
            var errors = new bool[model.Length];
            for (int i = 0; i < model.Length; i++)
            {
                var item = model[i];
                try
                {
                    _connection.Execute("[dbo].[sp_base_insert_short_clog_row]", 
                        new {
                            Id =id,
                            Code=item.Code,
                            NmoCode=item.NmoCode,
                            TicketCode=item.TicketCode,
                            Fio =item.Fio,
                            Organization =item.Organization,
                            Speciality =item.Speciality,
                            Phone=item.Phone,
                            Email=item.Email
                        },
                        commandType: CommandType.StoredProcedure);
                }
                catch (Exception e)
                {
                    if(i==1)
                    Console.WriteLine(e.Message);
                    errors[i] = true;
                }                
            }
            return errors;
        }
    }
}
