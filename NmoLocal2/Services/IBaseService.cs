using LenaSoft.Web.NmoLocal2.Models;

namespace LenaSoft.Web.NmoLocal2.Services
{
    public interface IBaseService
    {
        ConferenceModel[] GetConferences();
        ConferenceLogModel GetConferenceLogRow(int id);
        ConferenceLogModel[] GetConferenceLog(int id);
        void SetConferenceLog(ConferenceLogSetModel model);
        ConferenceLogModel[] Search(int id,string query);
        NmoCodeModel[] GetNmoCodes();
        bool[] InsertConferenceLogRows(ConferenceLogShortModel[] model, int id);
    }
}