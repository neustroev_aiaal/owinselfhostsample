﻿using System.Runtime.Serialization;
namespace LenaSoft.Web.NmoLocal2.SharedLib
{
    [DataContract]
    public class ApiResult
    {
        public ApiResult()
        {

        }
        public ApiResult(string code, string error)
        {
            this.Result = false;
            this.ErrorCode = code;
            this.Error = error;
        }
        [DataMember(Name = "result")]
        public bool Result { get; set; } = true;
        [DataMember(Name = "error")]
        public string Error { get; set; }
        [DataMember(Name = "errorCode")]
        public string ErrorCode { get; set; }
        [DataMember(Name = "obsolete")]
        public bool Obsolete { get; set; }
        [DataMember(Name = "itemId")]
        public int ItemId { get; set; }
    }
    [DataContract]
    public class ApiResult<T> : ApiResult
    {
        [DataMember(Name = "data")]
        public T Data;
      

        public ApiResult(T data) : base()
        {
            this.Data = data;
        }
        public ApiResult() : base()
        {
        }
       
    }

    
}
