﻿using System;

namespace LenaSoft.Web.NmoLocal2.SharedLib
{
    public class NmoException : Exception
    {
        public readonly string Code;

        public NmoException(string code,string message) : base(message)
        {
            Code = code;
        }

        public NmoException(string code, string message, Exception inner) : base(message, inner)
        {
            Code = code;
        }

        public ErrorData[] ErrorData { get; set; }
    }

    public class ErrorData
    {
        public ErrorData()
        {

        }
        public ErrorData(string errorCode, string error)
        {
            this.Error = error;
            this.ErrorCode = errorCode;
        }
        public string Error { get; set; }
        public string ErrorCode { get; set; }
        public string Subject { get; set; }
    }
}
