﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Microsoft.Owin.StaticFiles.Infrastructure;
using Owin;
using System.Net.Http.Headers;

namespace LenaSoft.Web.NmoLocal2
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();

            //использование аттрибутов Route
            config.MapHttpAttributeRoutes();

            //маршрут по умолчанию
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));

            appBuilder.UseWebApi(config);
            var root = ConfigurationManager.AppSettings["Http:Root"];
            var fsOptions = new FileServerOptions()
            {
                RequestPath = PathString.Empty,
                FileSystem = new PhysicalFileSystem(root),
                EnableDefaultFiles = true,
                EnableDirectoryBrowsing = false,
            };
            if(!fsOptions.DefaultFilesOptions.DefaultFileNames.Contains("index.html"))
                fsOptions.DefaultFilesOptions.DefaultFileNames.Add("index.html");

            appBuilder.UseFileServer(fsOptions);
        }
    }
}