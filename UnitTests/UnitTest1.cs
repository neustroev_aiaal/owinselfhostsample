﻿using System;
using System.IO;
using System.Net;
using System.Text;
using LenaSoft.Web.NmoLocal2.Models;
using LenaSoft.Web.NmoLocal2.SharedLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        public UnitTest1()
        {
            //LenaSoft.Web.NmoLocal2.Program.Main(null);
        }
        [TestMethod]
        public void GetConferences()
        {
            var req = WebRequest.Create("http://localhost:5000/api/base/1.0.0/get-conferences");
            var resp = req.GetResponse();
            var stream = resp.GetResponseStream();
            var reader = new StreamReader(stream, Encoding.UTF8);
            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult<ConferenceModel[]>>(reader.ReadToEnd());
            Assert.IsNotNull(result, "result != null");
            Assert.IsTrue(result.Result, "result.Result");
        }

        [TestMethod]
        public void GetConferenceLog()
        {
            var req = WebRequest.Create("http://localhost:5000/api/base/1.0.0/get-conferences");
            var resp = req.GetResponse();
            var stream = resp.GetResponseStream();
            var reader = new StreamReader(stream, Encoding.UTF8);
            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult<ConferenceModel[]>>(reader.ReadToEnd());
            Assert.IsNotNull(result, "result != null");
            Assert.IsTrue(result.Result,"result.Result");
            var item = result.Data[0];
            Assert.IsNotNull(item,"item != null");

            var req2 = WebRequest.Create("http://localhost:5000/api/base/1.0.0/get-clog-row?id="+item.Id);
            var stream2 = req2.GetRequestStream();
            var reader2 = new StreamReader(stream2);
            var log = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult<ConferenceLogModel>>(reader2.ReadToEnd());
            Assert.IsNotNull(log,"log != null");
        }
    }
}
